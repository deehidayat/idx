const update = (state, mutations) => {
	let result = Object.assign({}, state, mutations);
	result.data.map((record)=>{
	    record.previous_value = parseFloat(record.previous_value);
	    record.current_value = parseFloat(record.current_value);
	    
		// Selisih Hari ini dengan Hari kemarin
		record.diff = record.current_value - record.previous_value;

		// Hitung Persentase
	    if (record.previous_value == 0 || record.current_value == 0) {
	      record.percentage = 0;
	    } else {
    	  // harga hari ini dikurangi harga kemarin dibagi harga hari ini dikali 100
	      record.percentage =  (record.diff / record.current_value) * 100;
	    }

	    return record;
	});

	return result;
}

const initialState = {
  data: []
}

export default function reducers(state = initialState, action) {
	switch (action.type) {
		case 'LQ45_DATA_FETCHED':
		case 'LQ45_DATA_CHANGED':
			return update(state, action.payload)
			break
		default:
			return state
			break
	}
}