import React, { Component } from 'react'
import config from './../../utils/config'
import Header from './../../components/Header'
import Footer from './../../components/Footer'
import logo from './../../../images/bg-idxlogo.png'

export default class ClosingView extends Component {

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.pageTransition = setTimeout(() => {
      this.context.router.push('/')
    }, config.delayPerPageMin)
  }

  componentWillUnmount() {
    window.clearTimeout(this.pageTransition)
    this.pageTransition = undefined
  }

  render() {
    return(
      <div className="bgidx" style={{ height: '100%' }}>
          <a href="#" className="logo-idx">
              <img src={logo} className="img-responsive" alt="logo 4" />
          </a>
      </div>
    )
  }
}
