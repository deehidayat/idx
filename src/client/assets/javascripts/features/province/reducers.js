import { calculateVolume } from './../../utils/helper'

const initialState = {
  min: 0,
  max: 0,
  data: []
}

export default function reducers(state = initialState, action) {
	switch (action.type) {
		case 'PROVINCE_DATA_FETCHED':
      var max = _.maxBy(action.payload, 'value')
      var min = _.minBy(action.payload, 'value')

      return Object.assign({}, state, {
        ...state,
        min: min,
        max: max,
        data: action.payload
      })
			break
		default:
			return state
			break
	}
}
