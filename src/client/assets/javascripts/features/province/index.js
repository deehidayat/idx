import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Header from './../../components/Header4'
import Animator from './../../components/Animator'
import Indonesia from './../../components/MapOverlay/indonesia'
import ScreenBlokr from './../../components/ScreenBlokr'
import { Nett, BuySell } from './../../components/Legend4'
import config from './../../utils/config'

import * as actionCreators from './actions'

@connect(
  state => ({
    province: state.province
  }), dispatch => ({
    actions: bindActionCreators(actionCreators, dispatch)
  })
)
export default class ProvinceView extends Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      target: {
        name: 'Jakarta',
        lat: -6.277761,
        lon: 106.853027,
      }
    }
  }

  componentDidMount() {
    this.updateDataInterval = setInterval(() => {
      this.props.actions.updateData('east')
    }, config.dataUpdateInterval)

    this.props.actions.updateData('east')

    this.pageTransition = setTimeout(() => {
      this.context.router.push('/4b')
    }, config.delayPerPage)
  }

  componentWillUnmount() {
    window.clearInterval(this.updateDataInterval)
    window.clearTimeout(this.pageTransition)

    this.updateDataInterval = undefined
    this.pageTransition = undefined
  }

  sumProvinceNettValue(format) {
    let total = 0
    if (this.props.province.data) {
      total = _.reduce(this.props.province.data, (a, b) => {
        return a + b.value
      }, 0)
    }

    return total
  }

  sumValue(type, format) {
    let total = 0
    if (this.props.province.data) {
      total = _.reduce(this.props.province.data, (a, b) => {
        return a + b[type].value;
      }, 0)
    }

    if (typeof format == 'undefined')
      return parseFloat(total).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    else
      return total
  }

  render() {
    return(
      <div className="wrapper">
        <div className="section-top">
          <Header
            title="EAST INDONESIA"
            legends={this.state.legends}
            totalBuyValue={this.sumValue('buy')}
            totalSellValue={this.sumValue('sell')}
			//grandtotal= {this.sumValue('buy')} + {this.sumValue('sell')}
            showInfo={true}
            />
        </div>
        <Indonesia offset={-115}>
          <Animator data={this.props.province.data} target={this.state.target}>
            <BuySell data={this.props.province.data} target={this.state.target} />
          </Animator>
        </Indonesia>
        <ScreenBlokr />
      </div>
    )
  }
}
