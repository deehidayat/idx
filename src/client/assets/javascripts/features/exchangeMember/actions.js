import { CallAPI } from './../../utils/helper'


export function dataFetched(payload) {
	return {
		type: 'EXCHANGE_MEMBER_DATA_FETCHED',
		payload: payload
	}
}

export function updateData(payload) {
	return (dispatch, getState) => {
	    return CallAPI('exchange_member_transactions/sync', getState().foreign.data).then((response) => {
	    	dispatch(dataFetched(response.data))
	    })
	}
}