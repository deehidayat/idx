import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import config from './../../utils/config'
import * as actionCreators from './actions'
import Header from './../../components/Header'
import Footer from './../../components/Footer'
import ScreenBlokr from './../../components/ScreenBlokr'
import { numberFormat } from './../../utils/helper'
import ubs from './../../../images/ubs_logo.gif'
import ms from './../../../images/mandiri_sekuritas.jpg'
import si from './../../../images/sucor_invest.png'
import ps from './../../../images/phintraco_securities.png'
import './styles.scss'

/**
 * ExchangeMemberView [Page 5]
 */
@connect(
  state => ({
    exchangeMember: state.exchangeMember
  }),
  dispatch => ({
    actions: bindActionCreators(actionCreators, dispatch)
  })
)
export default class ExchangeMemberView extends Component {

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  componentDidMount() {
    this.pageTransition = setTimeout(() => {
      this.context.router.push('/6')
    }, config.delayPerPage)

    this.props.actions.updateData();

    this.updateDataInterval = setInterval(() => {
      this.props.actions.updateData()
    }, config.dataUpdateInterval)

  }

  componentWillUnmount() {
    window.clearTimeout(this.pageTransition)
    window.clearTimeout(this.updateDataInterval)
    this.pageTransition = undefined
    this.updateDataInterval = undefined
  }

  wrapNumber(number) {
      return number;
  }

  render() {
    return(
      <div className="wrapper">
        <div className="section-top">
          <Header
            title="Top 15 Exchange Members by Transaction Value"            
            showInfo="true"
            value={6345.77}
            totalBuyValue={ numberFormat(this.wrapNumber(this.props.exchangeMember.meta.buy_summary)) }
            totalSellValue={ numberFormat(this.wrapNumber(this.props.exchangeMember.meta.sell_summary)) }>
          </Header>

          <div className="mapsection">
            <div className="innercontent">
              <div className="container exchange_member">
                <div className="content">
                  <div className="row cnt-tvb">
                  { this.props.exchangeMember.data.map((member, i) => {
                    return (
                      <div className="col-md-3" key={i}>
                        <div className="box-tvb">
                          <h3 className="foot-point">{ i + 1 }</h3>
                          <div className="cimg">
                            <span className="helper"></span><img src={member.image} className="img-responsive" />
                          </div>
                          <h3 className="head-tvb">{ numberFormat(this.wrapNumber(member.buy)) }</h3>
                          <h3 className="foot-tvb">{ numberFormat(this.wrapNumber(member.sell)) }</h3>
                        </div>
                      </div>
                    )
                  })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <ScreenBlokr />
      </div>
    )
  }
}

              <div className="row cnt-tvb">
                <div className="col-md-3">
                  <div className="box-tvb">
                    <h3 className="head-tvb">3,422.00</h3>
                    <p>UBS Securities Indonesia</p>
                    <h3 className="foot-tvb">356.00</h3>
                  </div>
                </div>

              </div>
