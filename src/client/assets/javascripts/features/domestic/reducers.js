import { calculateVolume } from './../../utils/helper'

const update = (state, mutations) => Object.assign([], state, mutations)

const initialState = {
  buy: {
    min: 0,
    max: 0
  },
  sell: {
    min: 0,
    max: 0
  },
  data: []
}

export default function reducers(state = initialState, action) {
	switch (action.type) {
		case 'DOMESTIC_DATA_FETCHED':
			return Object.assign({}, state, {
        ...state,
        data: action.payload
      })
			break
		default:
			return state
			break
	}
}
