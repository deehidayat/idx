import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Header from './../../components/Header'
import Footer from './../../components/Footer'
import Indonesia from './../../components/MapOverlay/indonesia'
import Animator from './../../components/Animator'
import ScreenBlokr from './../../components/ScreenBlokr'
import { BuySell } from './../../components/Legend'
import * as actionCreators from './actions'
import config from './../../utils/config'
import './styles.scss'

@connect(
  state => ({
    domestic: state.domestic
  }),
  dispatch => ({
    actions: bindActionCreators(actionCreators, dispatch)
  })
)
export default class DomesticView extends Component {
  static childContextTypes = {
    projection: React.PropTypes.func.isRequired
  };

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      target: {
        name: 'Jakarta',
        lat: -6.299601,
        lon: 107.182617,
      }
    }
  }

  componentDidMount() {
    this.updateDataInterval = setInterval(() => {
      this.props.actions.updateData()
    }, config.dataUpdateInterval)

    this.pageTransition = setTimeout(() => {
      this.context.router.push('/4')
    }, config.delayPerPage)

  }

  componentWillMount() {
    this.props.actions.updateData()
  }

  componentWillUnmount() {
    window.clearInterval(this.updateDataInterval)
    window.clearTimeout(this.pageTransition)

    this.updateDataInterval = undefined
    this.pageTransition = undefined
  }

  randomizeBuyValue() {
    return (Math.random() * (10 - 5) + 5) * 10000
  }

  randomizeSellValue() {
    return (Math.random() * (5 - 1) + 1) * 10000
  }

  getVolumeFromTotal(change, point) {
    let vol = (change ? point.value + ((Math.random() * (10 - 5) + 5) * 100) : point.value) / this.sumValue('sell', false) * 10

    return vol < 1 ? 1 : (vol > 8 ? 8 : vol)
  }

  sumValue(type, format) {
    let total = 0
    if (this.props.domestic.data) {
      total = _.reduce(this.props.domestic.data, (a, b) => {
        return a + b[type].value
      }, 0)
    }

    if (typeof format == 'undefined')
      return parseFloat(total).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    else
      return total
  }

  getRandomData() {
    return _.map(this.props.domestic.data, (o) => {
      let shouldChange = {
        buy: Math.random() >= 0.5 ? 1 : 0,
        sell: Math.random() >= 0.5 ? 1 : 0
      }

      return Object.assign({}, o, {
        buy: {
          ...o.buy,
          value: (shouldChange.buy) ? o.buy.value + ((Math.random() * (10 - 5) + 5) * 100) : o.buy.value,
          change: shouldChange.buy,
        },
        sell: {
          ...o.sell,
          value: shouldChange.sell ? o.sell.value + ((Math.random() * (10 - 5) + 5) * 100) : o.sell.value,
          change: shouldChange.sell,
        }
      })
    })

    return data
  }

  render() {
    return(
      <div className="wrapper">
        <div className="section-top">
          <Header title="Domestic Transaction"
            legends={this.state.legends}
            showInfo="true"
            totalBuyValue={this.sumValue('buy')}
            totalSellValue={this.sumValue('sell')}
            value={6345.77}/>
          <Indonesia>
            <Animator data={this.props.domestic.data} target={this.state.target}>
              <BuySell data={this.props.domestic.data} precision={0} target={this.state.target} />
            </Animator>
          </Indonesia>
        </div>
        <ScreenBlokr />
      </div>
    )
  }
}
