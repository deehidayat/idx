import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import config from './../../utils/config'
import Header from './../../components/Header'
import Footer from './../../components/Footer'
import World from './../../components/MapOverlay/World'
import AnimatorNett from './../../components/Animator/nett'
import { Indexes } from './../../components/Legend'
import ScreenBlokr from './../../components/ScreenBlokr'

import * as actionCreators from './actions'

@connect(
  state => ({
    asean: state.asean
  }),
  dispatch => ({
    actions: bindActionCreators(actionCreators, dispatch)
  })
)
export default class AseanView extends Component {

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      target: {
        name: 'Jakarta',
        lat: -6.229064726587003,
        lon: 106.83790246508998,
      }
    }
  }

  componentDidMount() {
    this.pageTransition = setTimeout(() => {
      this.context.router.push('/')
    }, config.delayPerPage)

    this.updateDateInterval = setInterval(() => {
      this.props.actions.updateData()
    }, config.dataUpdateInterval)
  }

  componentWillMount() {
    this.props.actions.updateData()
  }

  componentWillUnmount() {
    window.clearTimeout(this.pageTransition)
    window.clearInterval(this.updateDateInterval)
    this.pageTransition = undefined
    this.updateDateInterval = undefined
  }

  getRandomData() {
    return _.map(this.props.asean, (o) => {
      let number = Math.random() * (4 - 0 + 1) + 0
      number = number < 0.5 ? 0 : number

      return Object.assign({}, o, {
        ...o,
        change: number.toFixed(2) * (Math.random() >= 0.5 ? 1 : -1),
      })
    })

    return data
  }

  render() {
    return(
      <div className="wrapper">
        <div className="section-top">
          <Header title="Major Bourse Index" value={6345.77}>
            <div className="innerscaption threeoption">
              <label className="btn-group" data-toggle="buttons">
                <span className="btn btn-success">
                  <input type="checkbox" />
                  <span className="glyphicon glyphicon-ok"></span>
                </span>
                <span className="labelnav">UP</span>
              </label>

              <label className="btn-group" data-toggle="buttons">
                <span className="btn btn-danger">
                  <input type="checkbox" />
                  <span className="glyphicon glyphicon-ok"></span>
                </span>
                <span className="labelnav">DOWN</span>
              </label>

              <label className="btn-group" data-toggle="buttons">
                <span className="btn btn-primary">
                  <input type="checkbox" />
                  <span className="glyphicon glyphicon-ok"></span>
                </span>
                <span className="labelnav">UNCHANGED</span>
              </label>
              <div className="clear"></div>
            </div>
          </Header>
        </div>
        <World offsetTop={150} offsetX={-150} marginTop={0}>
          <Indexes data={this.props.asean} target={this.state.target} />
        </World>
        <ScreenBlokr />
      </div>
    )
  }
}
