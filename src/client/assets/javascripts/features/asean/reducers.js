const update = (state, mutations) => Object.assign([], state, mutations)

const initialState = []

export default function reducers(state = initialState, action) {
	switch (action.type) {
		case 'ASEAN_DATA_FETCHED':
			return update(state, action.payload)
			break
		default:
			return state
			break
	}
}
