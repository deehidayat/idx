import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Footer from './../../components/Footer'
import { geoPath, select, geoEquirectangular, mouse } from 'd3'
import { feature } from 'topojson'
import SVGWorld from 'json!./json/countries.topojson'
import Animator from './../../components/Animator'
import config from './../../utils/config'
import Header from './../../components/Header'
import ScreenBlokr from './../../components/ScreenBlokr'
import { BuySell } from './../../components/Legend'

import * as actionCreators from './actions'

@connect(state => ({
    foreign: state.foreign
  }), dispatch => ({
    actions: bindActionCreators(actionCreators, dispatch)
  })
)
export default class ForeignView extends Component {

  static childContextTypes = {
    projection: React.PropTypes.any.isRequired
  };

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    this.target = {
      name: "Jakarta, ID",
      lat: -6.121435,
      lon: 106.774124
    }

    this.legends = [{
      name: 'Buy',
      class: 'squareBuy'
    }, {
      name: 'Sell',
      class: 'squareSell'
    }]

    this.projection = function() {
      return null
    }
  }

  getChildContext() {
    return {
      projection: this.projection
    }
  }

  componentWillMount() {
    this.updateDimensions()
    this.props.actions.updateData()
  }

  componentDidMount() {
    this.updateDataInterval = setInterval(() => {
      this.props.actions.updateData()
    }, config.dataUpdateInterval)

    this.draw()

    this.pageTransition = setTimeout(() => {
      this.context.router.push('/3')
    }, config.delayPerPage)
  }

  componentWillUnmount() {
    window.clearInterval(this.updateDataInterval)
    window.clearTimeout(this.pageTransition)
    this.pageTransition = undefined
    this.updateDataInterval = undefined
  }

  updateDimensions() {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight - 100
    });
  }

  draw() {
    let offsetTop = 75
    var svg1 = select("#map-wrapper").append("svg")
      .attr("width", this.state.width)
      .attr("height", this.state.height);

    this.projection = geoEquirectangular()
      .scale((this.state.width * (this.state.height / this.state.width)) / Math.PI)
      .translate([this.state.width / 2, (this.state.height / 2) + offsetTop])
      .rotate([-162, 0])

    var world_path = geoPath()
      .projection(this.projection);

    svg1
      .selectAll("path")
      // All the geospatial vector magic happens here
      .data(feature(SVGWorld, SVGWorld.objects.subunits).features)
      // For each topographic feature appent a path object
      .enter()
      .append("path")
      .attr("d", world_path)
      .style("fill", (d) => {
        if (d.id == 'IDN') {
          return '#AE292D';
        } else {
          return '#F4E2E3'
        }
      })
      .style("stroke-width", 0.5)
      .style("stroke", "white")

    let self = this

    if (!this.state.projectionReady)
      this.setState({projectionReady: true})
  }

  getVolumeFromTotal(change, point) {
    let vol = (change ? point.value + ((Math.random() * (10 - 5) + 5) * 100) : point.value) / this.sumValue('sell', false) * 10

    return vol < 1 ? 1 : (vol > 8 ? 8 : vol)
  }

  sumValue(type, format) {
    let total = 0
    if (this.props.foreign.data) {
      total = _.reduce(this.props.foreign.data, (a, b) => {
        return a + b[type].value
      }, 0)
    }

    if (typeof format == 'undefined')
      return parseFloat(total).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    else
      return total
  }

  getAnimator() {
    return (
      <Animator data={this.props.foreign.data} target={this.target} >
        <BuySell data={this.props.foreign.data} target={this.target} />
      </Animator>
    )
  }

  render() {

    let comp = this.state.projectionReady ? this.getAnimator() : null

    return (
      <div className="wrapper">
        <div className="section-top">
          <Header title="Foreign Transaction"
                  legends={this.legends}
                  totalBuyValue={this.sumValue('buy')}
                  totalSellValue={this.sumValue('sell')}
                  showInfo="true"
                  value={6345.77}/>
        </div>
        <div id="map-wrapper" className="map-wrapper"></div>
        <div className="canvas-wrapper">
          { comp }
        </div>

        <ScreenBlokr />
      </div>

    )
  }
}
