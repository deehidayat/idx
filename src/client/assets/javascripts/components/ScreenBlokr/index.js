import React, { Component } from 'react'

export default class ScreenBlokr extends Component {

	constructor() {
		super()

		this.state = {
			width: 0,
			height: 0
		}
	}

	updateDimensions() {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

  isMobile() { 
		if ( window.navigator.userAgent.match(/Android/i)
		|| window.navigator.userAgent.match(/webOS/i)
		|| window.navigator.userAgent.match(/iPhone/i)
		|| window.navigator.userAgent.match(/iPad/i)
		|| window.navigator.userAgent.match(/iPod/i)
		|| window.navigator.userAgent.match(/BlackBerry/i)
		|| window.navigator.userAgent.match(/Windows Phone/i)
		) {
			return true;
		}
		else {
			return false;
		}
	}

  componentWillMount() {
  	this.updateDimensions()
  }

  componentDidMount() {
    window.addEventListener("resize", () => this.updateDimensions());
  }

  componentWillUnmount() {
    window.removeEventListener("resize", () => this.updateDimensions());
  }

  showBlokr() {
  	return (
			<div className="screenBlokr" style={{
				width: this.state.width,
				height: this.state.height,
				background: '#fff',
				zIndex: 99999,
				position: 'absolute',
				top: 0,
				left: 0
			}}>
				<span style={{
					width: 500,
					textAlign: 'center',
					fontSize: 24,
					marginLeft: -250,
					marginTop: -40,
					position: 'absolute',
					top: '50%',
					left: '50%',
					background: 'white',
					padding: '10px'
				}}>
					This dashboard only can be seen on screen with resolution 1280x720 or above.
				</span>
			</div>
		)
  }

	render() {
		let component = (this.state.width < 1024 || this.isMobile()) ? this.showBlokr() : null
		return component
	}
}