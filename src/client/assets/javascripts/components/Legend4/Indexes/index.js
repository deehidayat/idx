import React, { Component } from 'react'
import _ from 'lodash'
import AnimateOnChange from 'react-animate-on-change'
import config from './../../../utils/config'
import { numberFormat } from './../../../utils/helper'

import 'font-awesome/css/font-awesome.css'

class IndexLegend extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return (this.props.point.change !== 0 && (this.props.point.change !== nextProps.point.change))
  }

  parseNumber(number) {
    return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }

  render() {

    const directionIcon = this.props.point.change == 0
        ? null
        : (this.props.point.change >= 0)
          ? <i className="fa fa-chevron-circle-up" style={{marginRight: 2}}></i>
          : <i className="fa fa-chevron-circle-down" style={{marginRight: 2}}></i>

    const styles = {
        position: 'absolute',
        top: this.props.start.y - 107,
        left: this.props.start.x,
        textAlign: 'center',
        display: 'block',
        width: 100,
        fontWeight: 'bold',
        fontSize: '16px',
        lineHeight: 1
      }

      if (this.props.point.labelPositioning) {
        styles.top += this.props.point.labelPositioning.top
        styles.left += this.props.point.labelPositioning.left
      }

      let color = this.props.point.change == 0
        ? config.color.unchanged
        : (this.props.point.change >= 0)
          ? config.color.buy
          : config.color.sell

      return (
        <span key={this.props.start.x + this.props.end.x} style={styles}>
          <span style={{ display: 'block', backgroundColor: '#E6E7E8' }}>
            <span style={{
              display: 'table-cell',
              width: styles.width,
              verticalAlign: 'middle',
              textTransform: 'uppercase',
            }}>
              <img src={ this.props.point.image } width="100" />
            </span>
          </span>
          <span style={{
            display: 'block',
            width: styles.width,
            backgroundColor: '#333',
            color: 'white',
            textAlign: 'center',
            padding: '8px 12px',
            fontSize: 14
          }}>
            {/* Change zero to this.props.point.value when backend integration completed */}
              { numberFormat(this.props.point.value, 0) }
          </span>
          <span style={{
            display: 'block',
            backgroundColor: color,
            padding: '12px 8px',
            color: '#fff',
            fontSize: 10
          }}>
            <span style={{
              display: 'block',
              float: 'left',
              textAlign: 'left'
            }}>
              <AnimateOnChange
              baseClassName="block"
              animationClassName="flash animated"
              animate={this.props.point.change != 0}>
                { directionIcon }
                { Math.abs(this.props.point.diff) }
              </AnimateOnChange>
            </span>
            <span style={{
              display: 'block',
              float: 'right',
              textAlign: 'right'
            }}>
              <AnimateOnChange
              baseClassName="block"
              animationClassName="flash animated"
              animate={this.props.point.change != 0}>
                { directionIcon } { this.props.point.change }%
              </AnimateOnChange>
            </span>
            <div className="clearfix"></div>
          </span>
        </span>
      )
  }
}

export default class Indexes extends Component {
	static contextTypes = {
		projection: React.PropTypes.func.isRequired
	}

	constructor(props) {
		super()
	}

  getImageUrl(point) {
    switch(point.flag) {
      case 'flagTH':
        return flagTH
      case 'flagID':
        return flagID
      case 'flagMY':
        return flagMY
      case 'flagPH':
        return flagPH
      case 'flagSG':
        return flagSG
    }
  }

	getPositionFromProjection(point) {
		let pos = this.context.projection([point.lon, point.lat])

		if (!pos) return null

		return {
			x: pos[0],
			y: pos[1]
		}
	}

	render() {
		const end = this.getPositionFromProjection(this.props.target)

		return (
			<div>
				{ this.props.data.map((point, i) => {
          const start = this.getPositionFromProjection(point)
          return <IndexLegend key={i} point={point} start={start} end={end} image={this.getImageUrl(point)} />
				})}
			</div>
		)
	}
}
