import React, { Component } from 'react'
import { Link } from 'react-router'

import './styles.scss'

export default class Footer extends Component {
  render() {
    return(
      <footer className="footer">
        <div className="copyright">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-3">
                <p>© 2017 - All Rights with Webenlance</p>
              </div>
              <div className="col-md-9">
                <ul className="bottom_ul">
                  <li><Link to={'/'}>Home</Link></li>
                  <li><Link to={'/foreign'}>Foreign</Link></li>
                  <li><Link to={'/domestic'}>Domestic</Link></li>
                  <li><Link to={'/province'}>Province</Link></li>
                  <li><Link to={'/tvbem'}>Exchange Members Transaction</Link></li>
                  <li><Link to={'/pmol45s'}>LQ 45 Shares</Link></li>
                  <li><Link to={'/asean'}>World Major Bourse Index</Link></li>
                  <li><Link to={'/closing'}>Close</Link></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}
