import React, { Component } from 'react'

import { drawCurvedLine, drawArrow, baseCircle, PointLegend } from './../CanvasDrawer'
import options from './../../utils/config'

export default class Animator extends Component {
  static contextTypes = {
    projection: React.PropTypes.any.isRequired
  }

  constructor() {
    super()

    this.t = 1
    this.state = {
      currentFps: 0
    }
  }

  componentDidMount() {
    this.fps = 30;
    this.now;
    this.then = Date.now();
    this.interval = 1000/this.fps;
    this.delta;

    this.animationId = window.requestAnimationFrame(this.init.bind(this))
    console.log("Animation ID #" + this.animationId + " attached.")
  }

  componentWillUnmount() {
    console.log("Unmounting...")
    if (this.animationId) {
      console.log("Animation ID #" + this.animationId + " detached.")
       window.cancelAnimationFrame(this.animationId);
       this.animationId = undefined;
    }

    this.FPSInterval = undefined
  }

  getPositionFromProjection(point) {
    let pos = this.context.projection([point.lon, point.lat])

    if (!pos) return null

    return {
      x: pos[0],
      y: pos[1]
    }
  }

  init() {
    this.ctx = this.refs.canvas.getContext('2d')
    this.draw()
    // this.showFps()
  }

  showFps() {
    this.FPSInterval = setInterval(() => {
      this.setState({
        currentFps: this.currentFps.toFixed(0)
      })
    }, 1000)
  }

  draw() {
    window.requestAnimationFrame(this.draw.bind(this))

    this.now = Date.now();
    this.delta = this.now - this.then;

    if (this.delta > this.interval) {
      this.currentFps = 1/(this.delta/1000)
      this.then = this.now - (this.delta % this.interval);
      this.ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);
      this.t += options.animationSpeed;

      if (this.t >= 2) {
        this.t = 1
      }

      this.props.data.map((point) => {
        let end = this.getPositionFromProjection(this.props.target)
        let start = this.getPositionFromProjection(point)

        let diffX = end.x - start.x
        let diffY = end.y - start.y

        // Calculate angle
        let angle = Math.atan2(diffY, diffX)

        // Path midpoint
        let midX = start.x + diffX * 0.5,
            midY = start.y + diffY * 0.5;

        let control = {
          x: midX + point.buy.radius * Math.sin(angle),
          y: midY - point.buy.radius * Math.cos(angle)
        }

        if (point.buy.change) {
          drawCurvedLine(this.ctx, start, end, control, point.buy)
        }

        if (point.sell.change) {
          if (point.sell) {
            let invertControl = {
              x: midX + point.sell.radius * Math.sin(angle),
              y: midY - point.sell.radius * Math.cos(angle)
            }


            drawCurvedLine(this.ctx, start, end, invertControl, point.sell, true)
          }
        }

        for (let i = 0; i < 200; i+=25) {
          if (point.buy.change) {
            drawArrow(this.ctx, start, control, end, point.buy, this.t - (i / 100))
          }

          if (point.sell && point.sell.change) {
            drawArrow(this.ctx, end, {
              x: midX + point.sell.radius * Math.sin(angle),
              y: midY - point.sell.radius * Math.cos(angle)
            }, start, point.sell, this.t - (i / 100), true)
          }
        }

        if (point.sell.change || point.buy.change) {
          baseCircle(this.ctx, start.x, start.y, point.buy.volume, '#3E7B40')
          baseCircle(this.ctx, end.x, end.y, 3, '#3E7B40')
        }
      })
    }
  }

  render() {
    return (
      <div className="drawer-wrapper">
        { this.props.children }
        <canvas ref="canvas" width={window.innerWidth} height={window.innerHeight}/>

        { (process.env.NODE_ENV !== 'production') &&
          <span style={{ position: 'absolute', bottom: 50, left: 10, fontWeight: 'bold', fontSize: 16 }}>{ this.state.currentFps } FPS</span>
        }
      </div>
    )
  }
}
