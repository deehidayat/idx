import React, { Component } from 'react'
import options from './../../utils/config'

import { drawCurvedLine, drawArrow, baseCircle, PointLegend } from './../CanvasDrawer'

/**
 * AnimatorNett
 */
export default class AnimatorNett extends Component {
  /**
   * [contextTypes description]
   * @type {Object}
   */
  static contextTypes = {
    projection: React.PropTypes.any.isRequired
  }

  /**
   * [constructor description]
   * @return {[type]} [description]
   */
  constructor() {
    super()
    this.t = 1
  }

  /**
   * [componentDidMount description]
   * @return {[type]} [description]
   */
  componentDidMount() {
    this.fps = 30;
    this.now;
    this.then = Date.now();
    this.interval = 1000/this.fps;
    this.delta;

    this.animationId = window.requestAnimationFrame(this.init.bind(this))
  }

  /**
   * [componentWillUnmount description]
   * @return {[type]} [description]
   */
  componentWillUnmount() {
    if (this.animationId) {
       window.cancelAnimationFrame(this.animationId);
       this.animationId = undefined;
    }

    this.FPSInterval = undefined
  }

  /**
   * [getPositionFromProjection description]
   * @param  {[type]} point [description]
   * @return {[type]}       [description]
   */
  getPositionFromProjection(point) {
    let pos = this.context.projection([point.lon, point.lat])
    if (!pos) return null

    return {
      x: pos[0],
      y: pos[1]
    }
  }

  /**
   * [init description]
   * @return {[type]} [description]
   */
  init() {
    this.ctx = this.refs.canvas.getContext('2d')
    this.draw()
  }

  /**
   * [draw description]
   * @return {[type]} [description]
   */
  draw() {
    window.requestAnimationFrame(this.draw.bind(this))

    this.now = Date.now();
    this.delta = this.now - this.then;

    if (this.delta > this.interval) {
      this.currentFps = 1/(this.delta/1000)
      this.then = this.now - (this.delta % this.interval);
      this.ctx.clearRect(0, 0, window.innerWidth, window.innerHeight);
      this.t += options.animationSpeed;

      if (this.t >= 2) {
        this.t = 1
      }

      this.props.data.map((point) => {
        let end = this.getPositionFromProjection(this.props.target)
        let start = this.getPositionFromProjection(point)

        let diffX = end.x - start.x
        let diffY = end.y - start.y

        // Calculate angle
        let angle = Math.atan2(diffY, diffX)

        // Path midpoint
        let midX = start.x + diffX * 0.5,
            midY = start.y + diffY * 0.5;

        let control = {
          x: midX + point.radius * Math.sin(angle),
          y: midY - point.radius * Math.cos(angle)
        }

        let inverse = point.change >= 0 ? undefined : true

        if (point.change) {
          drawCurvedLine(this.ctx, start, end, control, point, inverse)
        }

        for (let i = 0; i < 200; i+=25) {
          if (point.change > 0)
            drawArrow(this.ctx, start, control, end, point, this.t - (i / 100), inverse)
          else if (point.change < 0)
            drawArrow(this.ctx, end, control, start, point, this.t - (i / 100), inverse)
        }

        if (point.change) {
          baseCircle(this.ctx, start.x, start.y, point.volume ? point.volume : 2, '#3E7B40')
          baseCircle(this.ctx, end.x, end.y, 3, '#3E7B40')
        }
      })
    }
  }

  /**
   * [render description]
   * @return {[type]} [description]
   */
  render() {
    return (
      <div className="drawer-wrapper">
        { this.props.children }
        <canvas ref="canvas" width={window.innerWidth} height={window.innerHeight}/>
      </div>
    )
  }
}
