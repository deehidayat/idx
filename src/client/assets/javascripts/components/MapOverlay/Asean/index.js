import React, { Component } from 'react'
import { geoPath, select, geoEquirectangular, mouse, geoBounds, geoMercator, geoCentroid } from 'd3'
import { feature, mesh } from 'topojson'
import aseanJson from 'json!./asean.topojson'

export default class Asean extends Component {
  static childContextTypes = {
    projection: React.PropTypes.any.isRequired
  };

	constructor(props) {
		super(props)

		this.projection = function() {
      return null
    }
	}
	getChildContext() {
    return {
      projection: this.projection
    }
  }

  componentWillMount() {
    this.updateDimensions()
  }

  componentDidMount() {
    this.draw()
  }

  updateDimensions() {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

	draw() {	
    let offsetTop = 50
    let scale = 150

    let w = this.state.width,
        h = this.state.height - offsetTop,
        offset = [this.state.width/2, this.state.height/2]

    var layer = select("#map-wrapper").append("svg")
      .attr("width", w)
      .attr("height", h);

    var collection = feature(aseanJson, aseanJson.objects.collection);
    let center = geoCentroid(collection)

    this.projection = geoEquirectangular().scale(scale).center(center).translate(offset)

    let path = geoPath().projection(this.projection);

    let bounds = path.bounds(collection)
    let hscale  = scale*w  / (bounds[1][0] - bounds[0][0])
    let vscale  = scale*h / (bounds[1][1] - bounds[0][1])
    scale   = (hscale < vscale) ? hscale : vscale
    offset  = [
      w - (bounds[0][0] + bounds[1][0])/2,
      h - (bounds[0][1] + bounds[1][1])/2
    ]

    this.projection.scale(scale).center(center).translate([offset[0] - 100, offset[1]])

    layer.selectAll(".collection")
      .data(collection.features)
      .enter()
      .append("path")
      .attr("d", path)
      .style("fill", (d) => {
        return '#F4E2E3'
      })
      .style("stroke-width", 0.5)
      .style("stroke", "white")

    let self = this

    if (!this.state.projectionReady)
      this.setState({projectionReady: true})
  }

	render() {
    let animator = this.state.projectionReady ? this.props.children : null
		
		return (
			<div>
				<div id="map-wrapper" className="map-wrapper"></div>
				{ animator }
			</div>
		)
	}
}