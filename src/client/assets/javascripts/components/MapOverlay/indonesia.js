import React, { Component } from 'react'
import { geoPath, select, geoEquirectangular, mouse } from 'd3'
import { feature, mesh } from 'topojson'
import indonesiaJSON from './indonesia.json'

export default class Indonesia extends Component {
	constructor(props) {
		super(props)

		this.projection = function() {
      return null
    }
	}
	getChildContext() {
    return {
      projection: this.projection
    }
  }

  componentWillMount() {
    this.updateDimensions()
  }

  componentDidMount() {
    this.draw()
  }

  updateDimensions() {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }

	draw() {
    let width = this.state.width,
        height = this.state.height;

    var layer = select("#map-wrapper").append("svg")
      .attr("width", width)
      .attr("height", height);

    var subunits = feature(indonesiaJSON, indonesiaJSON.objects.subunits);
    var provinces = feature(indonesiaJSON, indonesiaJSON.objects.states_provinces);

    this.projection = geoEquirectangular()
      .scale(width)
      .rotate([this.props.offset ? this.props.offset : -119, 0])
      .translate([width / 2, (height / 2) - 100]);

    var path = geoPath().projection(this.projection);

    layer.append("path")
      .datum(subunits)
      .attr("d", path);

    layer.selectAll(".subunit")
      .data(subunits.features)
      .enter()
      .append("path")
      .attr("d", path)
      .style("fill", (d) => {
        if (d.id == 'IDN') {
          return '#F4E2E3';
        } else {
          return '#F3F3F5'
        }
      })
      .style("stroke-width", 0.5)
      .style("stroke", "white")

    layer
      .selectAll('path')
      .data(provinces.features)
      .enter()
      .append("path")
      .attr("d", path)
      .style('fill', (d) => {
        if (d.properties.code_hasc == 'ID.JK') {
          return '#AE292D'
        }
      })
      .attr("class", "city-border");

    let self = this

    layer.on("mousedown.log", function() {
      console.log(mouse(this), self.projection.invert(mouse(this)));
    });

    if (!this.state.projectionReady)
      this.setState({projectionReady: true})
  }

	render() {
    let animator = this.state.projectionReady ? this.props.children : null

		return (
			<div>
				<div id="map-wrapper" className="map-wrapper"></div>
				{ animator }
			</div>
		)
	}
}

Indonesia.childContextTypes = {
  projection: React.PropTypes.any.isRequired
};
