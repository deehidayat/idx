import { API_URL } from './../env'

const options = {
  API_URL: process.env.NODE_ENV !== 'production' ? API_URL : API_URL,
	delayPerPage: 60000,
	delayPerPageMin: 10000,
	animationSpeed: 0.01,
	dataUpdateInterval: 2500,
	color: {
		buy: '#57ba59',
		sell: '#C02C30',
		unchanged: '#00A3F1'
	}
}

export default options
