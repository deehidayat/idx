import React from 'react';
import { Route, IndexRoute, Redirect } from 'react-router';

import App from './App';
import HomeView from './../features/home'
import ForeignView from './../features/foreign'
import DomesticView from './../features/domestic'
import ExchangeMemberView from './../features/exchangeMember'
import LQ45 from './../features/LQ45'
import ClosingView from './../features/closing'
import ProvinceViewEast from './../features/province'
import ProvinceViewWest from './../features/province/west-side'
import AseanView from './../features/asean'

import NotFoundView from 'components/NotFound';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomeView} />
    <Route path="/2" component={ForeignView} />
    <Route path="/3" component={DomesticView} />
    <Route path="/4" component={ProvinceViewEast} />
    <Route path="/4b" component={ProvinceViewWest} />
    <Route path="/5" component={ExchangeMemberView} />
    <Route path="/6" component={LQ45} />
    <Route path="/7" component={AseanView} />
    <Route path="/8" component={ClosingView} />
    <Route path="404" component={NotFoundView} />
    <Redirect from="*" to="404" />
  </Route>
);
