import React, { Component, PropTypes } from 'react';
import _ from 'lodash'
import config from './../utils/config'

class App extends Component {
	constructor(props) {
		super(props)
		this.pages = ['/', '/2', '/3', '/4', '/4b', '/5', '/6']
	}

	componentDidMount() {
		let incrementPage = (jump) => {
			let current_path = this.props.location.pathname
			let index = _.findIndex(this.pages, (page) => page == current_path)

			if (index + jump > this.pages.length - 1)
				index = 0;
			else if (index + jump < 0)
				index = this.pages.length - 1;
			else
				index += jump;

			this.context.router.push(this.pages[index])
		}

		// Disabled, handled by each page
		// this.pageChangeState = setInterval(() => {
		// 	// incrementPage(1);
		// }, config.delayPerPage);

		/**
		 * Supaya bisa pindah page manual
		 */
		window.addEventListener('keydown', (e) => {
			if (e.keyCode === 39) {
				incrementPage(1)
			} else if (e.keyCode === 37) {
				incrementPage(-1)
			}
		});
	}

	componentWillUnmount() {
		window.clearInterval(this.pageChangeState)
	}

	render() {
		return (
		  <div className="page-container">
		    {React.cloneElement({...this.props}.children, {...this.props})}
		  </div>
		)
	}
}

App.propTypes = {
  children: PropTypes.element.isRequired
};

App.contextTypes = {
	router: PropTypes.object.isRequired
}

export default App;
