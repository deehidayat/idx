/**
 * Created by faerulsalamun on 2/16/16.
 */

'use strict';

const mongoose = require(`mongoose`);
const config = require(`../../config/${process.env.NODE_ENV || ``}`);

const timestamp = require(`./plugins/Timestamp`);

const Schema = mongoose.Schema;
const TradeForeignSchema = new Schema({

  id: false,

  datas: [
    {
      name: {
        type: String,
      },
      lat: {
        type: Number,
      },
      lon: {
        type: Number,
      },
      buy: {
        radius: {
          type: Number,
        },
        value: {
          type: Number,
        },
        change: {
          type: Number,
        },
        volume: {
          type: Number,
        },
      },
      sell: {
        radius: {
          type: Number,
        },
        value: {
          type: Number,
        },
        change: {
          type: Number,
        },
        volume: {
          type: Number,
        },
      },
      labelPositioning: {
        top: {
          type: Number,
        },
        left: {
          type: Number,
        },
      },
    },
  ],


}, {
  collection: config.collection.name(`trade_foreigns`),
  toObject: {
    virtuals: true,
  },
  toJSON: {
    virtuals: true,
  },
});

TradeForeignSchema.plugin(timestamp.useTimestamps);

module.exports = mongoose.model(`TradeForeign`, TradeForeignSchema);
