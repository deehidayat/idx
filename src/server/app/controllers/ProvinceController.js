/**
 * Created by faerulsalamun on 1/18/17.
 */


const _ = require(`lodash`);
const moment = require(`moment-timezone`);

// Services
const Async = require(`../services/Async`);
const Utils = require(`../services/Utils`);

// Models
const TradeProvince = require(`../models/TradeProvince`);

module.exports = {

  getSyncDataApi: Async.route(function* (req, res, next) {
    const dataDay = Utils.getDay();
    const tomorrow = moment(dataDay).endOf(`days`).format(`YYYY-MM-DDTHH:mm:ss`);

    let datas = yield TradeProvince.findOne({
      createdTime: {
        $gte: dataDay,
        $lt: tomorrow,
      },
    }).lean().exec();

    if (!datas) {
      // Get latest data
      datas = yield TradeProvince.findOne()
        .limit(1)
        .sort({ _id: -1 })
        .lean()
        .exec();
    }

    if (datas) {
      _.map(datas.datas, (obj) => {
        const dataOldBuy = Utils.checkNotNullValue(_.find(req.body, { name: obj.name }))
          ? _.find(req.body, { name: obj.name }).buy.value
          : 0;

        obj.buy.change = obj.buy.value - dataOldBuy;

        const dataOldSell = Utils.checkNotNullValue(_.find(req.body, { name: obj.name }))
          ? _.find(req.body, { name: obj.name }).sell.value
          : 0;

        obj.sell.change = obj.sell.value - dataOldSell;
      });

      // const dataBuy = {
      //   max: _.maxBy(datas.datas, o => o.buy.value),
      //   min: _.minBy(datas.datas, o => o.buy.value),
      // };
      //
      // const dataSell = {
      //   max: _.maxBy(datas.datas, o => o.sell.value),
      //   min: _.minBy(datas.datas, o => o.sell.value),
      // };
      //
      //
      // for (let i = 0, length = datas.datas.length; i < length; i += 1) {
      //   datas.datas[i].buy.volume =
      //     Utils.getCalculateVolume(datas.datas[i].buy.value, dataBuy.max[`buy`].value, dataBuy.min[`buy`].value, 2);
      //   datas.datas[i].sell.volume =
      //     Utils.getCalculateVolume(datas.datas[i].sell.value, dataSell.max[`sell`].value, dataSell.min[`sell`].value, 2);
      // }


      if (Utils.checkNotNullValue(req.query.side)) {
        _.remove(datas.datas, data => data.side !== req.query.side);
      }

      return res.ok(datas.datas);
    }

    return res.ok([]);
  }),

};
