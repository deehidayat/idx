/**
 * Created by faerulsalamun on 1/24/17.
 */

const _ = require(`lodash`);
const moment = require(`moment-timezone`);

// Services
const Async = require(`../services/Async`);
const Utils = require(`../services/Utils`);

// Models
const TradeExchangeMemberTransaction = require(`../models/TradeExchangeMemberTransaction`);

module.exports = {

  getSyncDataApi: Async.route(function* (req, res, next) {
    const dataDay = Utils.getDay();
    const tomorrow = moment(dataDay).endOf(`days`).format(`YYYY-MM-DDTHH:mm:ss`);

    let datas = yield TradeExchangeMemberTransaction.findOne({
      createdTime: {
        $gte: dataDay,
        $lt: tomorrow,
      },
    }).lean().exec();

    let dataMeta = {
      buy_summary: 0,
      sell_summary: 0,
    };

    if (!datas) {
      // Get latest data
      datas = yield TradeExchangeMemberTransaction.findOne()
        .limit(1)
        .sort({ _id: -1 })
        .lean()
        .exec();
    }

    if (datas) {
      dataMeta = {
        buy_summary: datas.buy_summary,
        sell_summary: datas.sell_summary,
      };

      return res.ok(datas.datas, `Ok`, 200, dataMeta);
    }


    return res.ok([], `Ok`, 200, dataMeta);
  }),

};
