/**
 * Created by faerulsalamun on 1/24/17.
 */

const _ = require(`lodash`);

// Services
const Async = require(`../services/Async`);
const Utils = require(`../services/Utils`);
const config = require(`./../../config/${process.env.NODE_ENV || ``}`);

module.exports = {
  getSyncDataApi: Async.route(function* (req, res, next) {
    const width = 100;
    const height = 107;
    const margin = { top: 8, left: 8 };

    const datas = [
      // Group TH
      {
        name: `stock-exchange-of-thailand`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/stock-exchange-of-thailand.jpg`,
      },
      {
        name: `bursa-malaysia.jpg`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/bursa-malaysia.jpg`,
        labelPositioning: {
          left: width + margin.left,
          top: 0,
        },
      },
      {
        name: `singapore-exchange.jpg`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/singapore-exchange.jpg`,
        labelPositioning: {
          left: (width * 2) + (margin.left * 2),
          top: 0,
        },
      },
      {
        name: `philippine-stock-exchange.jpg`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/philippine-stock-exchange.jpg`,
        labelPositioning: {
          left: (width * 3) + (margin.left * 3),
          top: 0,
        },
      },
      {
        name: `hongkong-exchange.jpg`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/hongkong-exchange.jpg`,
        labelPositioning: {
          top: ((height) + (margin.top)) * -1,
          left: 0,
        },
      },
      {
        name: `taiwan-stock-exchange.jpg`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/taiwan-stock-exchange.jpg`,
        labelPositioning: {
          top: ((height) + (margin.top)) * -1,
          left: width + margin.left,
        },
      },
      {
        name: `shanghai-stock-exchange.jpg`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/shanghai-stock-exchange.jpg`,
        labelPositioning: {
          top: ((height * 2) + (margin.top * 2)) * -1,
          left: 0,
        },
      },
      {
        name: `korea-exchange.jpg`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/korea-exchange.jpg`,
        labelPositioning: {
          top: ((height * 2) + (margin.top * 2)) * -1,
          left: width + margin.left,
        },
      },
      {
        name: `japan-exchange-group.jpg`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/japan-exchange-group.jpg`,
        labelPositioning: {
          top: ((height * 2) + (margin.top * 2)) * -1,
          left: (width * 2) + (margin.left * 2),
        },
      },
      {
        name: `shenzen-stock-exchange.jpg`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/shenzen-stock-exchange.jpg`,
        labelPositioning: {
          top: ((height * 2) + (margin.top * 2)) * -1,
          left: (width + margin.left) * -1,
        },
      },
      {
        name: `indonesia-stock-exchange.jpg`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/indonesia-stock-exchange.jpg`,
        labelPositioning: {
          top: ((height * 2) + (margin.top * 2)),
          left: 0,
        },
      },
      {
        name: `australian-security-exchange.jpg`,
        lat: 11.426187,
        lon: 98.964844,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/australian-security-exchange.jpg`,
        labelPositioning: {
          top: ((height * 2) + (margin.top * 2)),
          left: ((width * 2) + (margin.left * 2)),
        },
      },
      // Group US
      {
        name: `nyse.jpg`,
        lat: 42.996612,
        lon: -110.039063,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/nyse.jpg`,
      },
      {
        name: `nasdaq-us.jpg`,
        lat: 42.996612,
        lon: -110.039063,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/nasdaq-us.jpg`,
        labelPositioning: {
          top: height + margin.top,
          left: 0,
        },
      },
      // Group London (NASDAQ Nordiq)
      {
        name: `nasdaq-nordic.jpg`,
        lat: 47.450380,
        lon: 1.406250,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/nasdaq-nordic.jpg`,
      },
      {
        name: `oslo-bors.jpg`,
        lat: 47.450380,
        lon: 1.406250,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/oslo-bors.jpg`,
        labelPositioning: {
          top: (height + (margin.top)),
          left: 0,
        },
      },
      {
        name: `london-stock-exchange-group.jpg`,
        lat: 47.450380,
        lon: 1.406250,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/london-stock-exchange-group.jpg`,
        labelPositioning: {
          top: ((height * 2) + (margin.top * 2)),
          left: 0,
        },
      },
      {
        name: `euronext.jpg`,
        lat: 47.450380,
        lon: 1.406250,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/euronext.jpg`,
        labelPositioning: {
          left: width + margin.left,
          top: ((height + margin.top) / 2) * -1,
        },
      },
      {
        name: `deutsche-borse-group.jpg`,
        lat: 47.450380,
        lon: 1.406250,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/deutsche-borse-group.jpg`,
        labelPositioning: {
          left: width + margin.left,
          top: (height + margin.top) / 2,
        },
      },
      {
        name: `luxembourg-stock-exchange.jpg`,
        lat: 47.450380,
        lon: 1.406250,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/luxembourg-stock-exchange.jpg`,
        labelPositioning: {
          left: width + margin.left,
          top: (height * 2 + margin.top * 2) - ((height + margin.top) / 2),
        },
      },
      {
        name: `swiss-exchange.jpg`,
        lat: 47.450380,
        lon: 1.406250,
        image: `${req.protocol}:\\\\${req.get(`host`)}/img/swiss-exchange.jpg`,
        labelPositioning: {
          left: width + margin.left,
          top: (height * 3 + margin.top * 3) - ((height + margin.top) / 2),
        },
      },
    ];

    const indexes = req.body.length ? req.body : datas;

    const result = _.map(indexes, (data) => {
      const direction = Math.random() >= 0.5 ? 1 : -1;
      const change = (Math.floor(Math.random() * (0 - 100 + 1)) + 100) * direction;
      const value = typeof data.value === `undefined` ? Math.floor(Math.random() * (500 - 1000 + 1)) + 1000 : data.value + change;
      const changePercentage = (change / value) * 100;

      return Object.assign({}, data, {
        value,
        diff: change,
        change: changePercentage.toFixed(2),
      });
    });

    res.ok(result);
  }),

};
